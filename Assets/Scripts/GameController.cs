﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    GameObject selectedCharacter;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //if (Input.GetMouseButtonDown(0)) {
        if (Input.touchCount == 1) {
            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                RaycastHit hit = new RaycastHit();
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Transform objectHit;

                if (Physics.Raycast(ray, out hit))
                {

                    objectHit = hit.transform;


                    if (objectHit.GetComponentInParent<CharacterBehaviour>() != null)
                    {
                        objectHit.gameObject.GetComponentInParent<CharacterBehaviour>().GetCurrentCell().PerformAction();
                    }
                    else if(objectHit.GetComponent<CharacterBehaviour>() != null)
                    {
                        objectHit.gameObject.GetComponent<CharacterBehaviour>().GetCurrentCell().PerformAction();
                    }       
                    else if (objectHit.GetComponent<CellProperty>() != null)
                    {
                        objectHit.GetComponent<CellProperty>().PerformAction();
                    }
                }
            }

        }
    }
}
