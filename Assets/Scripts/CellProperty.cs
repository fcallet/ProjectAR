﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public enum CellState {Default, Wall, Trapped};

public class CellProperty : MonoBehaviour {

    //Columns
    public int x;
    //Rows
    public int y;
    //Index in the array
    public int indexInList;

    //Cell color
    public Material defaultMaterial;
    public Material greenMaterial;
    public Material redMaterial;

    //State of the cell  in the game
    public CellState state;

    //Current Character
    private CharacterBehaviour currentCharacter;


    //Wall mesh
    private Wall wall;
    //Smoke particle system
    private Smoke smoke;

    private Action<CellProperty> actionToPerform;

    //for navigation
    public int fScore;

    private bool visible;

    void Start ()
    {
        wall = GetComponentInChildren<Wall>(true);
        smoke = GetComponentInChildren<Smoke>(true);
    }
	
	// Update is called once per frame
	void Update () {
	}

    public void HighlightGreen()
    {
        GetComponent<MeshRenderer>().material = greenMaterial;
    }

    public void HighlightRed()
    {
        GetComponent<MeshRenderer>().material = redMaterial;
    }

    public void DisableHighlight()
    {
        GetComponent<MeshRenderer>().material = defaultMaterial;
    }

    public void SetAction(Action<CellProperty> action)
    {
        actionToPerform = action;
    }

    public void PerformAction()
    {
        if (actionToPerform != null)
        {
            actionToPerform(this);
        }
    }

    public void SetCurrentCharacter(CharacterBehaviour character)
    {
        currentCharacter = character;
    }

    public CharacterBehaviour GetCurrentCharacter()
    {
        return currentCharacter;
    }

    public bool IsInList(List<CellProperty> list)
    {
        foreach (CellProperty cell in list)
        {
            if (cell == this)
                return true;
        }
        return false;
    }

    public void SetVisibility(bool isVisible)
    {
        visible = isVisible;

        smoke.gameObject.SetActive(!visible);
        if (state == CellState.Wall)
        {
            wall.gameObject.SetActive(visible);
            //wall.GetComponent<NavMeshObstacle>().enabled = visible;
        }

        else if (state == CellState.Default)
        {
            if(currentCharacter != null)
                currentCharacter.gameObject.SetActive(visible);
        }
 
    }

    public bool IsVisible()
    {
        return visible;
    }

    public void DealDamage(int amount)
    {
        if (currentCharacter != null)
            currentCharacter.DealDamage(amount);
    }
}
