﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { NORTH, EAST, WEST, SOUTH, SOUTHEAST, SOUTHWEST, NORTHEAST, NORTHWEST}

public class GameManager : MonoBehaviour {

    public PlayerController redPlayer;
    public PlayerController bluePlayer;

    public UIManager ui;

    public int nbOFRows;
    public int nbOfColumns;

    private int turnNumber = 0;
    private const int placementSize = 3;

    private CharacterBehaviour selectedCharacter;
    private CellProperty[] cells;

    private List<CellProperty> cellsToUnmask;
    private CellProperty collisionCell;

    // Use this for initialization
    void Start () {
        selectedCharacter = null;
        cells = GetComponentsInChildren<CellProperty>();
        cellsToUnmask = new List<CellProperty>();
        StartCoroutine(WaitBeforeInit());
    }

    private IEnumerator WaitBeforeInit()
    {
        yield return new WaitForEndOfFrame();
        InitBoard();
        //BeginInitiation(redPlayer);
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void InitBoard()
    {
        for (int i = 0; i  < cells.Length; i ++)
        {
            cells[i].x = (int)(nbOfColumns * nbOFRows - (i + 1)) / nbOFRows;
            cells[i].y = (nbOfColumns * nbOFRows - (i + 1)) % nbOfColumns;
            cells[i].indexInList = i;
            cells[i].SetVisibility(false);
        }
    }

    public void BeginInitiation(PlayerController player)
    {
        InitArcher(player);
    }



    public void InitArcher(PlayerController player)
    {
        ui.BeginPlacingArcher(player);
        bool canBePlacedHere = false;
        for (int i = 0; i < cells.Length; i++)
        {
            canBePlacedHere = (player == redPlayer) && (cells[i].indexInList % nbOFRows < placementSize) 
                          ||  (player == bluePlayer) && (cells[i].indexInList % nbOFRows >= nbOFRows - placementSize) ? true : false;
            if (canBePlacedHere && cells[i].state != CellState.Wall && cells[i].GetCurrentCharacter() == null)
            {
                cells[i].SetAction(player.InitArcher);
                cells[i].HighlightGreen();
            }
        }
    }

    public void InitAssassin(PlayerController player)
    {
        ui.BeginPlacingAssassin(player);
        bool canBePlacedHere = false;
        for (int i = 0; i < cells.Length; i++)
        {
            canBePlacedHere = (player == redPlayer) && (cells[i].indexInList % nbOFRows < placementSize)
                          || (player == bluePlayer) && (cells[i].indexInList % nbOFRows >= nbOFRows - placementSize) ? true : false;
            if (canBePlacedHere && cells[i].state != CellState.Wall && cells[i].GetCurrentCharacter() == null)
            {
                cells[i].SetAction(player.InitAssassin);
                cells[i].HighlightGreen();
            }
        }
    }

    public void InitWarrior(PlayerController player)
    {
        ui.BeginPlacingWarrior(player);
        bool canBePlacedHere = false;
        for (int i = 0; i < cells.Length; i++)
        {
            canBePlacedHere = (player == redPlayer) && (cells[i].indexInList % nbOFRows < placementSize)
                          || (player == bluePlayer) && (cells[i].indexInList % nbOFRows >= nbOFRows - placementSize) ? true : false;
            if (canBePlacedHere && cells[i].state != CellState.Wall && cells[i].GetCurrentCharacter() == null)
            {
                cells[i].SetAction(player.InitWarrior);
                cells[i].HighlightGreen();
            }
        }
    }

    public void InitKing(PlayerController player)
    {
        ui.BeginPlacingKing(player);
        bool canBePlacedHere = false;
        for (int i = 0; i < cells.Length; i++)
        {
            canBePlacedHere = (player == redPlayer) && (cells[i].indexInList % nbOFRows < placementSize)
                          || (player == bluePlayer) && (cells[i].indexInList % nbOFRows >= nbOFRows - placementSize) ? true : false;
            if (canBePlacedHere && cells[i].state != CellState.Wall && cells[i].GetCurrentCharacter() == null)
            {
                cells[i].SetAction(player.InitKing);
                cells[i].HighlightGreen();
            }
        }
    }


    public void ConfirmInitiation(PlayerController player)
    {
        ClearAllActions();
        if (player == redPlayer)
        {
            ResetVisiblity();
        }
        else
        {
            BeginTurn(redPlayer);
        }
    }

    private void ClearAllActions()
    {
        for (int i = 0; i < cells.Length; i++)
        {
            cells[i].SetAction(null);
            cells[i].DisableHighlight();
        }
    }

    public void BeginSelectionAt(CellProperty cell)
    {
       
        CharacterBehaviour character = cell.GetCurrentCharacter();
        if (selectedCharacter != null)
        {
            EndSelectionAt(selectedCharacter.GetCurrentCell());
        }
        character.LoadPlayerPanelContent();
        selectedCharacter = character;
        character.SetOutline(true);
        character.SetAnimationState(AnimationState.LookAround);
        character.ShowMovementPossibilities();
        cell.SetAction(EndSelectionAt);
        

    }


    public void EndSelectionAt(CellProperty cell)
    {
    
        CharacterBehaviour character = cell.GetCurrentCharacter();
        character.SetOutline(false);
        character.SetAnimationState(AnimationState.Idle);
        character.HideAllPossibilites();
        cell.SetAction(BeginSelectionAt);
        selectedCharacter = null;

    }

    public void BeginTurn(PlayerController player)
    {
        if (selectedCharacter != null)
            EndSelectionAt(selectedCharacter.GetCurrentCell());
        ResetVisiblity();
        ClearAllActions();
        ui.LoadPlayerPanel(player);
        player.ResetMovementPoints();
        if (player == redPlayer)
        {
            turnNumber++;
        }
        ui.SetNewRoundNumber(turnNumber);
        //recompute visibility and player begin turn will be done once another player click "start" button, to solve visibility issue for both players
        //RecomputeVisibilityForPlayer(player);
        //player.BeginTurn();
    }

    public void UnmaskVisibilityForNewTurn(PlayerController player)
    {
        RecomputeVisibilityForPlayer(player);
        UnmaskVisibilityOfCells();
        player.BeginTurn();
    }

    public CellProperty[] GetCells()
    {
        return cells;
    }

    public Pair<int, int> GetBoardDimension()
    {
        return new Pair<int, int>(nbOFRows, nbOfColumns);
    }

    public void RecomputeVisibilityForPlayer(PlayerController player)
    {
        ResetVisiblity();
        player.ComputeVisibility();
        UnmaskVisibilityOfCells();


    }

    public void ResetVisiblity()
    {
        foreach(CellProperty cell in cells)
        {
            cell.SetVisibility(false);
        }
    }

    public void OnTangibleTargetCollision(CellProperty cell)
    {
        collisionCell = cell;
        cellsToUnmask = new List<CellProperty>();
        foreach (CellProperty aCell in cells)
        {
            if (aCell.y == collisionCell.y)
            {
                aCell.HighlightGreen();
                cellsToUnmask.Add(aCell);
            } else
            {
                aCell.DisableHighlight();
                cellsToUnmask.Remove(aCell);

            }
        }
        Debug.Log("cell collision : " + cell.x + ", " + cell.y);
    }

    public void UnmaskVisibilityOfCells()
    {
        if(cellsToUnmask.Count != 0)
        {
            Debug.Log("should unmask cells");
            foreach (CellProperty cell in cellsToUnmask)
            {
                cell.DisableHighlight();
                cell.SetVisibility(true);
            }
        }
    }

    public void OnFinishTurn()
    {
        cellsToUnmask = new List<CellProperty>();
    }


}
