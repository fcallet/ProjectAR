﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingBehaviour : CharacterBehaviour {

    public Material kingDefaultMaterial;
    public Material kingOutlinedMaterial;
    public Material kingCrownDefaultMaterial;
    public Material kingCrownOutlinedMaterial;

    private SkinnedMeshRenderer[] kingMeshRenderers;
    private List<CellProperty> attackPossibilities;
    private CellProperty targetCell;

    // Use this for initialization
    protected override void Start () {
        base.Start();
        maxMovementPoints = 1;
        maxHealth = 6;
        currentHealth = maxHealth;
        currentMovementPoints = maxMovementPoints;
        kingMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
    }
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    public override void SetOutline(bool isOutlined)
    {
        if(isOutlined)
        {
            for(int i = 0; i < kingMeshRenderers.Length - 1; i++)
            {
                kingMeshRenderers[i].material = kingOutlinedMaterial;
            }
            kingMeshRenderers[kingMeshRenderers.Length - 1].material = kingCrownOutlinedMaterial;
        }
        else
        {
            for (int i = 0; i < kingMeshRenderers.Length - 1; i++)
            {
                kingMeshRenderers[i].material = kingDefaultMaterial;
            }
            kingMeshRenderers[kingMeshRenderers.Length - 1].material = kingCrownDefaultMaterial;
        }
    }

    public void ShowAttackPossibilities()
    {
        HideAllPossibilites();
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        attackPossibilities = FindFistAttackPossibilities(board, dimension);
    }

    public void HideAttackPossibilities()
    {
        if (attackPossibilities != null)
        {
            foreach (CellProperty cell in attackPossibilities)
            {
                cell.DisableHighlight();
                if (cell.GetCurrentCharacter() == null || !owner.IsCharacterInTeam(cell.GetCurrentCharacter()))
                    cell.SetAction(null);
            }
        }
    }

    public void AttackWithFist(CellProperty target)
    {
        transform.LookAt(target.GetCurrentCharacter().transform);
        HideAllPossibilites();
        animator.SetBool("Attack", true);
        targetCell = target;
    }

    public void AttackPlayer()
    {
        const int amount = 1;
        targetCell.GetCurrentCharacter().DealDamage(amount);
        targetCell = null;
    }

    public void StopFistAttackAnimation()
    {
        animator.SetBool("Attack", false);
        currentMovementPoints = 0;
        ui.ResetContentUI();
    }

    public override void HideAllPossibilites()
    {
        base.HideAllPossibilites();
        HideAttackPossibilities();
    }

    private List<CellProperty> FindFistAttackPossibilities(CellProperty[] board, Pair<int, int> dimension)
    {
        List<CellProperty> possibilities = new List<CellProperty>();
        int indexNorth = (currentCell.indexInList % dimension.First != 9) ? currentCell.indexInList + 1 : -1;
        int indexSouth = (currentCell.indexInList % dimension.First != 0) ? currentCell.indexInList - 1 : -1;
        int indexWest = (currentCell.indexInList >= dimension.First) ? currentCell.indexInList - dimension.First : -1;
        int indexEast = (dimension.First * dimension.Second - currentCell.indexInList > dimension.First) ? currentCell.indexInList + dimension.First : -1;
        int indexNorthEast = (indexNorth != -1 && indexEast != -1) ? currentCell.indexInList + dimension.First + 1 : -1;
        int indexNorthWest = (indexNorth != -1 && indexWest != -1) ? currentCell.indexInList - dimension.First + 1 : -1;
        int indexSouthWest = (indexSouth != -1 && indexWest != -1) ? currentCell.indexInList - dimension.First - 1 : -1;
        int indexSouthEast = (indexSouth != -1 && indexEast != -1) ? currentCell.indexInList + dimension.First - 1 : -1;

        if (indexNorth != -1)
        {
            if (board[indexNorth].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorth].GetCurrentCharacter()))
            {
                board[indexNorth].HighlightGreen();
                board[indexNorth].SetAction(AttackWithFist);
            }
            else
            {
                board[indexNorth].HighlightRed();
            }
            possibilities.Add(board[indexNorth]);

        }
        if (indexSouth != -1)
        {
            if (board[indexSouth].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouth].GetCurrentCharacter()))
            {
                board[indexSouth].HighlightGreen();
                board[indexSouth].SetAction(AttackWithFist);
            }
            else
            {
                board[indexSouth].HighlightRed();
            }
            possibilities.Add(board[indexSouth]);
        }
        if (indexWest != -1)
        {
            if (board[indexWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexWest].GetCurrentCharacter()))
            {
                board[indexWest].HighlightGreen();
                board[indexWest].SetAction(AttackWithFist);
            }
            else
            {
                board[indexWest].HighlightRed();
            }
            possibilities.Add(board[indexWest]);
        }
        if (indexEast != -1)
        {
            if (board[indexEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexEast].GetCurrentCharacter()))
            {
                board[indexEast].HighlightGreen();
                board[indexEast].SetAction(AttackWithFist);
            }
            else
            {
                board[indexEast].HighlightRed();
            }
            possibilities.Add(board[indexEast]);
        }
        if (indexNorthEast != -1)
        {
            if (board[indexNorthEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorthEast].GetCurrentCharacter()))
            {
                board[indexNorthEast].HighlightGreen();
                board[indexNorthEast].SetAction(AttackWithFist);
            }
            else
            {
                board[indexNorthEast].HighlightRed();
            }
            possibilities.Add(board[indexNorthEast]);
        }
        if (indexNorthWest != -1)
        {
            if (board[indexNorthWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorthWest].GetCurrentCharacter()))
            {
                board[indexNorthWest].HighlightGreen();
                board[indexNorthWest].SetAction(AttackWithFist);
            }
            else
            {
                board[indexNorthWest].HighlightRed();
            }
            possibilities.Add(board[indexNorthWest]);
        }
        if (indexSouthEast != -1)
        {
            if (board[indexSouthEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouthEast].GetCurrentCharacter()))
            {
                board[indexSouthEast].HighlightGreen();
                board[indexSouthEast].SetAction(AttackWithFist);
            }
            else
            {
                board[indexSouthEast].HighlightRed();
            }
            possibilities.Add(board[indexSouthEast]);
        }
        if (indexSouthWest != -1)
        {
            if (board[indexSouthWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouthWest].GetCurrentCharacter()))
            {
                board[indexSouthWest].HighlightGreen();
                board[indexSouthWest].SetAction(AttackWithFist);
            }
            else
            {
                board[indexSouthWest].HighlightRed();
            }
            possibilities.Add(board[indexSouthWest]);
        }



        return possibilities;
    }

    public override void Die()
    {
        base.Die();
        ui.GameOver();
    }
}
