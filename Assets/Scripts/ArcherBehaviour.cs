﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherBehaviour : CharacterBehaviour {

    public Material archerDefaultMaterial;
    public Material archerOutlinedMaterial;
    private Material archerDefaultBowMaterial;
    public Material archerOutlinedBowMaterial;
    public Material archerOutlinedSkinMaterial;

    private SkinnedMeshRenderer[] archerMeshRenderers;

    public Arrows arrows;
    private const int range = 4;
    private List<CellProperty> attackPossibilities;

    private CellProperty currentTarget;

    // Use this for initialization
    protected override void Start () {
        base.Start();
        maxHealth = 5;
        maxMovementPoints = 2;
        currentHealth = maxHealth;
        currentMovementPoints = maxMovementPoints;
        archerMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        archerDefaultBowMaterial = archerMeshRenderers[1].material;
    }
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    public override void SetOutline(bool isOutlined)
    {
        if(isOutlined)
        {
            archerMeshRenderers[1].material = archerOutlinedBowMaterial;
            archerMeshRenderers[2].material = archerOutlinedMaterial;
        }
        else
        {
            archerMeshRenderers[1].material = archerDefaultBowMaterial;
            archerMeshRenderers[2].material = archerDefaultMaterial;
        }
    }

    public override void SetCurrentCell(CellProperty cell)
    {
        base.SetCurrentCell(cell);
        arrows.transform.position = transform.position + new Vector3(0.0f, 0.015f, 0.0f);
    }

    public void ShowBowAttackPossibilities()
    {
        HideMovementPossibilities();
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        attackPossibilities = FindBowAttackPossibilites(range, board, dimension);
    }

    private void HideBowAttackPossibilities()
    {
        if (attackPossibilities != null)
        {
            foreach (CellProperty cell in attackPossibilities)
            {
                cell.DisableHighlight();
                if(cell.GetCurrentCharacter() == null || !owner.IsCharacterInTeam(cell.GetCurrentCharacter()))
                    cell.SetAction(null);
            }
        }
    }

    private List<CellProperty> FindBowAttackPossibilites(int currentRange, CellProperty[] board, Pair<int, int> dimension, CellProperty visitedCell = null, List<CellProperty> cellsVisited = null)
    {

        if (cellsVisited == null)
            cellsVisited = new List<CellProperty>();

        if (currentRange >= 0)
        {

            if (visitedCell != null && visitedCell.IsVisible() && visitedCell.GetCurrentCharacter() != null && !owner.IsCharacterInTeam(visitedCell.GetCurrentCharacter()))
            {
                visitedCell.HighlightGreen();
                visitedCell.SetAction(LaunchArrowOnCell);
                if (!visitedCell.IsInList(cellsVisited))
                    cellsVisited.Add(visitedCell);
            }
            else if (visitedCell != null && visitedCell.GetCurrentCharacter() != this)
            {
                visitedCell.HighlightRed();
                if (!visitedCell.IsInList(cellsVisited))
                    cellsVisited.Add(visitedCell);
            }
            else
                visitedCell = currentCell;

            int indexNorth = (visitedCell.indexInList % dimension.First != 9) ? visitedCell.indexInList + 1 : -1;
            int indexSouth = (visitedCell.indexInList % dimension.First != 0) ? visitedCell.indexInList - 1 : -1;
            int indexWest = (visitedCell.indexInList >= dimension.First) ? visitedCell.indexInList - dimension.First : -1;
            int indexEast = (dimension.First * dimension.Second - visitedCell.indexInList > dimension.First) ? visitedCell.indexInList + dimension.First : -1;


            if (indexNorth != -1)
                FindBowAttackPossibilites(currentRange - 1, board, dimension, board[indexNorth], cellsVisited);
            if (indexSouth != -1)
                FindBowAttackPossibilites(currentRange - 1, board, dimension, board[indexSouth], cellsVisited);
            if (indexWest != -1)
                FindBowAttackPossibilites(currentRange - 1, board, dimension, board[indexWest], cellsVisited);
            if (indexEast != -1)
                FindBowAttackPossibilites(currentRange - 1, board, dimension, board[indexEast], cellsVisited);
        }

        return cellsVisited;
    }

    public void StopAttackWithBowAnimation()
    {
        arrows.Launch(currentTarget);
        currentTarget = null;
        SetAttackWithBowAnimation(false);
        currentMovementPoints = 0;
        ui.ResetContentUI();
    }

    public void SetAttackWithBowAnimation(bool attack)
    {
        animator.SetBool("Attack", attack);
    }

    public void LaunchArrowOnCell(CellProperty cell)
    {
        transform.LookAt(cell.GetCurrentCharacter().transform);
        transform.localEulerAngles += new Vector3(0, 15, 0);
        currentTarget = cell;
        SetAttackWithBowAnimation(true);
        HideBowAttackPossibilities();
    }

    public override void HideAllPossibilites()
    {
        base.HideAllPossibilites();
        HideBowAttackPossibilities();
    }
}
