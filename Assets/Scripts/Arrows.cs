﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrows : MonoBehaviour {

    private CellProperty target;
    private Animation arrowAnimation;
    private Transform arrowTransform;

    private AnimationClip animationClip;

    private bool rotate;
    private Vector3 arrowSpeed;
    private Vector3 lastPos;

    private Vector3 initialPosition;
    private Quaternion initialRotation;

    // Use this for initialization
	void Start () {
        arrowAnimation = GetComponent<Animation>();
        arrowTransform = GetComponent<Transform>();
        rotate = false;
        initialRotation = arrowTransform.localRotation;
        gameObject.SetActive(false);
	}
	

    void Update()
    {
        if(rotate && lastPos != arrowTransform.position)
        {
            arrowSpeed = arrowTransform.position - lastPos;
            arrowSpeed /= Time.deltaTime;
            lastPos = arrowTransform.position;
            arrowTransform.rotation = Quaternion.LookRotation(arrowSpeed);
        }
    }

    public void Launch(CellProperty cell)
    {
        gameObject.SetActive(true);
        if (animationClip != null)
            arrowAnimation.RemoveClip(animationClip);

        initialPosition = arrowTransform.localPosition;
        target = cell;

        AnimationClip clip = new AnimationClip();
        clip.name = "LaunchClip";
        clip.legacy = true;
        Vector3 distanceVector = (target.transform.position - arrowTransform.position);
        AnimationCurve curve_x = AnimationCurve.Linear(0, arrowTransform.localPosition.x, 1, arrowTransform.localPosition.x + distanceVector.x);
        AnimationCurve curve_y = AnimationCurve.EaseInOut(0, arrowTransform.localPosition.y, 1, arrowTransform.localPosition.y + distanceVector.y);
        curve_y.AddKey(0.5f, arrowTransform.localPosition.y + distanceVector.y / 2 + 0.018f);
        AnimationCurve curve_z = AnimationCurve.Linear(0, arrowTransform.localPosition.z, 1, arrowTransform.localPosition.z + distanceVector.z);
        clip.SetCurve("", typeof(Transform), "localPosition.x", curve_x);
        clip.SetCurve("", typeof(Transform), "localPosition.y", curve_y);
        clip.SetCurve("", typeof(Transform), "localPosition.z", curve_z);

        AnimationEvent animationEvent = new AnimationEvent();
        animationEvent.time = 1.0f;
        animationEvent.functionName = "LaunchCountdownPositionReset";
        clip.AddEvent(animationEvent);

        arrowAnimation.clip = clip;
        arrowAnimation.AddClip(clip, clip.name);
        animationClip = clip;

        lastPos = arrowTransform.position;
        rotate = true;

        arrowAnimation.Play();
    }

    private void LaunchCountdownPositionReset()
    {
        rotate = false;
        target.DealDamage(2);
        StartCoroutine(WaitBeforePositionReset());
    }

    private IEnumerator WaitBeforePositionReset()
    {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
        arrowTransform.localPosition = initialPosition;
        arrowTransform.localRotation = initialRotation;
    }
}
