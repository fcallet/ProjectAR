﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public enum AnimationState { Idle, LookAround, Hit, Death};

public class CharacterBehaviour : MonoBehaviour {

    public PlayerController owner;
    public UIManager ui;
    private GameManager gameManager;
    private Transform selfTransform;

    protected Animator animator;
    
    private AnimationState animationState;

    protected CellProperty currentCell;

    protected int maxHealth;
    protected int currentHealth;
    protected int maxMovementPoints;
    protected int currentMovementPoints;
    protected DamageUI damageUI;

    private const int maxVisibilityDepth = 3;

    private List<CellProperty> movementPossibilities;

    //for navigation
    private float speed = 0.02f;
    private int leftSteps = -1;
    private bool isNavigating;
    private List<CellProperty> pathFound = new List<CellProperty>();
    private Transform target;
    private bool isTargetSet;
    private bool setDoneNavigationEffect;

    // Use this for initialization
    protected virtual void Start ()
    {
        gameManager = GetComponentInParent<GameManager>();
        currentCell = null;
        animator = GetComponent<Animator>();
        
        selfTransform = GetComponent<Transform>();
        damageUI = GetComponentInChildren<DamageUI>();
        animationState = AnimationState.Idle;
        leftSteps = -1;
        isNavigating = false;
        setDoneNavigationEffect = false;
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        if (isNavigating)
        {
            //animator.SetTrigger("LookAround");
            //lookAround = false;
            //isWalking = false;
            //Debug.Log("one step");
            
            if (leftSteps > 0 && !isTargetSet)
            {
                target = pathFound.ElementAt(pathFound.Count - leftSteps).transform;
                Debug.Log("target is set : " + pathFound.ElementAt(pathFound.Count - leftSteps).x + ", " + pathFound.ElementAt(pathFound.Count - leftSteps).y);
                selfTransform.LookAt(target);
                leftSteps --;
                isTargetSet = true;
            }
            
            Debug.Log("distance : " + Vector3.Distance(selfTransform.position, target.transform.position));
            if (Vector3.Distance(selfTransform.position, target.transform.position) < 0.0035)
            {
                isTargetSet = false;
                if(leftSteps == 0)
                {
                    isNavigating = false;
                    setDoneNavigationEffect = true;
                }

            } else
            {
                selfTransform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }

        }
        if (setDoneNavigationEffect)
        {
            SetCurrentCell(pathFound.Last());
            pathFound.Last().SetCurrentCharacter(this);
            HideMovementPossibilities();
            ShowMovementPossibilities();
            SetIsWalking(false);

            setDoneNavigationEffect = false;
            gameManager.RecomputeVisibilityForPlayer(owner);
            ui.ResetContentUI();
            //ui.UpdateMovementPointValue(currentMovementPoints);
        }
    }

    public void SetIdleState()
    {
        SetAnimationState(AnimationState.Idle);
    }

    public void SetAnimationState(AnimationState animationState)
    {
        this.animationState = animationState;
        if (this.animationState == AnimationState.Idle)
            animator.SetTrigger("Idle");
        else if (this.animationState == AnimationState.LookAround)
            animator.SetTrigger("LookAround");
        else if (this.animationState == AnimationState.Hit)
            animator.SetTrigger("Hit");
        else if (this.animationState == AnimationState.Death)
            animator.SetTrigger("Death");
    }

    private void SetIsWalking(bool isWalking)
    {
        animator.SetBool("Walking", isWalking);
    }


    public virtual void SetCurrentCell(CellProperty cell)
    {
        currentCell = cell;
    }

    public CellProperty GetCurrentCell()
    {
        return currentCell;
    }

    public virtual void SetOutline(bool isOutlined) { }

    public virtual void ShowMovementPossibilities()
    {
        HideAllPossibilites();
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        movementPossibilities = FindMovementPossibilites(currentMovementPoints, board, dimension);
    }

    public void HideMovementPossibilities()
    {
        if (movementPossibilities != null)
        {
            for(int i = 0; i < movementPossibilities.Count; i++)
            {
                movementPossibilities[i].DisableHighlight();
                movementPossibilities[i].SetAction(null);
            }
            movementPossibilities = null;
        }
    }

    private List<CellProperty> FindMovementPossibilites(int currentMovementPoints, CellProperty[] board, Pair<int, int> dimension, CellProperty visitedCell = null, List<CellProperty> cellsVisited = null)
    {
        
        if(cellsVisited == null)
            cellsVisited = new List<CellProperty>();

        if(currentMovementPoints >= 0)
        {

            if (visitedCell != null)
            {
                visitedCell.HighlightGreen();
                visitedCell.SetAction(Navigate);
                if (!visitedCell.IsInList(cellsVisited))
                    cellsVisited.Add(visitedCell);
            }
            else
                visitedCell = currentCell;

            int indexNorth = (visitedCell.indexInList % dimension.First != 9) ? visitedCell.indexInList + 1 : -1;
            int indexSouth = (visitedCell.indexInList % dimension.First != 0) ? visitedCell.indexInList - 1 : -1;
            int indexWest = (visitedCell.indexInList >= dimension.First) ? visitedCell.indexInList - dimension.First : -1;
            int indexEast = (dimension.First * dimension.Second - visitedCell.indexInList > dimension.First) ? visitedCell.indexInList + dimension.First : -1;
             

            if (indexNorth != -1 && board[indexNorth].state != CellState.Wall && board[indexNorth].GetCurrentCharacter() == null)
                FindMovementPossibilites(currentMovementPoints - 1, board, dimension, board[indexNorth], cellsVisited);
            if (indexSouth != -1 && board[indexSouth].state != CellState.Wall && board[indexSouth].GetCurrentCharacter() == null)
                FindMovementPossibilites(currentMovementPoints - 1, board, dimension, board[indexSouth], cellsVisited);
            if (indexWest != -1 && board[indexWest].state != CellState.Wall && board[indexWest].GetCurrentCharacter() == null)
                FindMovementPossibilites(currentMovementPoints - 1, board, dimension, board[indexWest], cellsVisited);
            if (indexEast != -1 && board[indexEast].state != CellState.Wall && board[indexEast].GetCurrentCharacter() == null)
                FindMovementPossibilites(currentMovementPoints - 1, board, dimension, board[indexEast], cellsVisited);
        }

        return cellsVisited;
    }

    public void Navigate(CellProperty selectedCell)
    {
        SetIsWalking(true);
        FindPath(selectedCell);
        leftSteps = pathFound.Count;
        //leftSteps--;
        //selfTransform.LookAt(pathFound.ElementAt(pathFound.Count - leftSteps).transform.position);
        isNavigating = true;
        //agent.SetDestination(pathFound.ElementAt(pathFound.Count - leftSteps).transform.position);
        currentCell.SetCurrentCharacter(null);
        currentCell = selectedCell;
        currentMovementPoints -= pathFound.Count;
        
        //lookAround = true;
        //isWalking = true;

    }

    private void FindPath(CellProperty selectedCell)
    {
  
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        List<CellProperty> openList = new List<CellProperty>();
        List<CellProperty> closedList = new List<CellProperty>();
        Dictionary<CellProperty, CellProperty> cameFrom = new Dictionary<CellProperty, CellProperty>();
        CellProperty pathingCell;
        CellProperty destinationCell = selectedCell;
        openList.Add(currentCell);
        do
        {
            foreach(CellProperty cell in openList)
            {
                CalculateFScore(currentCell, cell, destinationCell);
            }
            pathingCell = FindCellWithLowestFScore(openList, closedList);
            closedList.Add(pathingCell);
            openList.Remove(pathingCell);
            if (closedList.Contains(destinationCell))
            {
                break;
            }
            List<CellProperty> adjacentCells = GetAccessibleAdjacentCells(pathingCell, board, dimension);
            foreach (CellProperty aCell in adjacentCells)
            {
                if (closedList.Contains(aCell))
                {
                    continue;
                }
                if (!openList.Contains(aCell))
                {
                    
                    CalculateFScore(currentCell, aCell, destinationCell);
                    cameFrom.Add(aCell, pathingCell);
                    openList.Add(aCell);
                }
                else
                {
                    
                }
            }
        }
        while (openList.Count != 0);

        pathFound = GetPathCells(selectedCell, cameFrom);
    }

    
    private int CalculateDistance(CellProperty cellA, CellProperty cellB)
    {
        int score = System.Math.Abs(cellA.x - cellB.x) + System.Math.Abs(cellA.y - cellB.y);
        return score;
    }

    private void CalculateFScore(CellProperty currentCell, CellProperty pathingCell, CellProperty destinationCell)
    {
        int gScore = System.Math.Abs(currentCell.x - pathingCell.x) + System.Math.Abs(currentCell.y - pathingCell.y);
        int hScore = System.Math.Abs(destinationCell.x - pathingCell.x) + System.Math.Abs(destinationCell.y - pathingCell.y);
        pathingCell.fScore = gScore + hScore;
    }

    private CellProperty FindCellWithLowestFScore(List<CellProperty> openList, List<CellProperty> closedList)
    {
        int score = 1000;
        CellProperty cellFound = openList.ElementAt(0);
        foreach (CellProperty cell in openList)
        {
            if(cell.fScore <= score && closedList.Count != 0)
            {
                int distanceCellToPathingCell = System.Math.Abs(cell.x - closedList.Last().x) + System.Math.Abs(cell.y - closedList.Last().y);
                int distanceCellFoundToPathingCell = System.Math.Abs(cellFound.x - closedList.Last().x) + System.Math.Abs(cellFound.y - closedList.Last().y);
                if(distanceCellToPathingCell <= distanceCellFoundToPathingCell)
                {
                    cellFound = cell;
                    score = cellFound.fScore;
                }
            }
        }
        return cellFound;
    }

    private List<CellProperty> GetAccessibleAdjacentCells(CellProperty targetCell, CellProperty[] board, Pair<int, int> dimension)
    {
        List<CellProperty> accessibleAdjacentCells = new List<CellProperty>();
        int indexNorth = (targetCell.indexInList % dimension.First != 9) ? targetCell.indexInList + 1 : -1;
        int indexSouth = (targetCell.indexInList % dimension.First != 0) ? targetCell.indexInList - 1 : -1;
        int indexWest = (targetCell.indexInList >= dimension.First) ? targetCell.indexInList - dimension.First : -1;
        int indexEast = (dimension.First * dimension.Second - targetCell.indexInList > dimension.First) ? targetCell.indexInList + dimension.First : -1;

        if (indexNorth != -1 && board[indexNorth].state != CellState.Wall && board[indexNorth].GetCurrentCharacter() == null)
            accessibleAdjacentCells.Add(board[indexNorth]);
        if (indexWest != -1 && board[indexWest].state != CellState.Wall && board[indexWest].GetCurrentCharacter() == null)
            accessibleAdjacentCells.Add(board[indexWest]);
        if (indexSouth != -1 && board[indexSouth].state != CellState.Wall && board[indexSouth].GetCurrentCharacter() == null)
            accessibleAdjacentCells.Add(board[indexSouth]);
        if (indexEast != -1 && board[indexEast].state != CellState.Wall && board[indexEast].GetCurrentCharacter() == null)
            accessibleAdjacentCells.Add(board[indexEast]);
 
        return accessibleAdjacentCells;
    }

    private List<CellProperty> GetPathCells(CellProperty destinationCell, Dictionary<CellProperty, CellProperty> cameFrom)
    {

        List<CellProperty> path = new List<CellProperty>();
        CellProperty current = destinationCell;

        while (!current.Equals(currentCell))
        {
            if (!cameFrom.ContainsKey(current))
            {
                Debug.Log("cameFrom does not contain current.");
                return new List<CellProperty>();
            }
            path.Add(current);
            current = cameFrom[current];
        }

        path.Reverse();
        foreach(CellProperty cell in path)
        {
            Debug.Log("Path : " + cell.x + " " + cell.y);
        }
        return path;
    }

    public void ShowVisibleCells()
    {
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        List<Direction> firstExpansionRules = new List<Direction>();
        firstExpansionRules.Add(Direction.NORTH);
        firstExpansionRules.Add(Direction.EAST);
        firstExpansionRules.Add(Direction.NORTHEAST);
        firstExpansionRules.Add(Direction.NORTHWEST);
        firstExpansionRules.Add(Direction.SOUTH);
        firstExpansionRules.Add(Direction.SOUTHEAST);
        firstExpansionRules.Add(Direction.SOUTHWEST);
        firstExpansionRules.Add(Direction.WEST);
        FindVisibleCells(currentCell, board, dimension, firstExpansionRules, maxVisibilityDepth);
    }

    public void FindVisibleCells(CellProperty visitedCell, CellProperty[] board, Pair<int,int> dimension, List<Direction> expansionRules, int depth)
    {
        visitedCell.SetVisibility(true);
        if(visitedCell.state != CellState.Wall && depth > 0)
        {
            foreach(Direction expansionRule in expansionRules)
            {
                ExpandVisibleCellSearch(visitedCell, board, dimension, expansionRule, depth);

            }
        }
    }

    public void ExpandVisibleCellSearch(CellProperty visitedCell, CellProperty[] board, Pair<int, int> dimension, Direction expansionRule, int depth)
    {
        List<Direction> nextExpandRules = new List<Direction>();
        if (expansionRule == Direction.NORTH)
        {
            int indexNorth = (visitedCell.indexInList % dimension.First != 9) ? visitedCell.indexInList + 1 : -1;
            if (indexNorth != -1)
            {
                nextExpandRules.Add(Direction.NORTH);
                FindVisibleCells(board[indexNorth], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if(expansionRule == Direction.SOUTH)
        {
            int indexSouth = (visitedCell.indexInList % dimension.First != 0) ? visitedCell.indexInList - 1 : -1;
            if (indexSouth != -1)
            {
                nextExpandRules.Add(Direction.SOUTH);
                FindVisibleCells(board[indexSouth], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if (expansionRule == Direction.EAST)
        {
            int indexEast = (dimension.First * dimension.Second - visitedCell.indexInList > dimension.First) ? visitedCell.indexInList + dimension.First : -1;
            if (indexEast != -1)
            {
                nextExpandRules.Add(Direction.EAST);
                FindVisibleCells(board[indexEast], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if (expansionRule == Direction.WEST)
        {
            int indexWest = (visitedCell.indexInList >= dimension.First) ? visitedCell.indexInList - dimension.First : -1;
            if (indexWest != -1)
            {
                nextExpandRules.Add(Direction.WEST);
                FindVisibleCells(board[indexWest], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if (expansionRule == Direction.NORTHEAST)
        {
            int indexNorth = (visitedCell.indexInList % dimension.First != 9) ? visitedCell.indexInList + 1 : -1;
            int indexEast = (dimension.First * dimension.Second - visitedCell.indexInList > dimension.First) ? visitedCell.indexInList + dimension.First : -1;
            int indexNorthEast = (indexNorth != -1 && indexEast != - 1) ? visitedCell.indexInList + dimension.First + 1 : -1;
            if (indexNorth != -1)
            {
                nextExpandRules.Add(Direction.NORTH);
                FindVisibleCells(board[indexNorth], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexEast != -1)
            {
                nextExpandRules.Add(Direction.EAST);
                FindVisibleCells(board[indexEast], board, dimension, nextExpandRules, depth - 1);
            }
            if(indexNorthEast != -1)
            {
                nextExpandRules.Add(Direction.EAST);
                nextExpandRules.Add(Direction.NORTH);
                nextExpandRules.Add(Direction.NORTHEAST);
                FindVisibleCells(board[indexNorthEast], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if (expansionRule == Direction.NORTHWEST)
        {
            int indexNorth = (visitedCell.indexInList % dimension.First != 9) ? visitedCell.indexInList + 1 : -1;
            int indexWest = (visitedCell.indexInList >= dimension.First) ? visitedCell.indexInList - dimension.First : -1;
            int indexNorthWest = (indexNorth != -1 && indexWest != -1) ? visitedCell.indexInList - dimension.First + 1 : -1;
            if (indexNorth != -1)
            {
                nextExpandRules.Add(Direction.NORTH);
                FindVisibleCells(board[indexNorth], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexWest != -1)
            {
                nextExpandRules.Add(Direction.WEST);
                FindVisibleCells(board[indexWest], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexNorthWest != -1)
            {
                nextExpandRules.Add(Direction.WEST);
                nextExpandRules.Add(Direction.NORTH);
                nextExpandRules.Add(Direction.NORTHWEST);
                FindVisibleCells(board[indexNorthWest], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if(expansionRule == Direction.SOUTHWEST)
        {
            int indexSouth = (visitedCell.indexInList % dimension.First != 0) ? visitedCell.indexInList - 1 : -1;
            int indexWest = (visitedCell.indexInList >= dimension.First) ? visitedCell.indexInList - dimension.First : -1;
            int indexSouthWest = (indexSouth != -1 && indexWest != -1) ? visitedCell.indexInList - dimension.First - 1 : -1;
            if (indexSouth != -1)
            {
                nextExpandRules.Add(Direction.SOUTH);
                FindVisibleCells(board[indexSouth], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexWest != -1)
            {
                nextExpandRules.Add(Direction.WEST);
                FindVisibleCells(board[indexWest], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexSouthWest != -1)
            {
                nextExpandRules.Add(Direction.WEST);
                nextExpandRules.Add(Direction.SOUTH);
                nextExpandRules.Add(Direction.SOUTHWEST);
                FindVisibleCells(board[indexSouthWest], board, dimension, nextExpandRules, depth - 1);
            }
        }
        else if (expansionRule == Direction.SOUTHEAST)
        {
            int indexSouth = (visitedCell.indexInList % dimension.First != 0) ? visitedCell.indexInList - 1 : -1;
            int indexEast = (dimension.First * dimension.Second - visitedCell.indexInList > dimension.First) ? visitedCell.indexInList + dimension.First : -1;
            int indexSouthEast = (indexSouth != -1 && indexEast != -1) ? visitedCell.indexInList + dimension.First - 1 : -1;
            if (indexSouth != -1)
            {
                nextExpandRules.Add(Direction.SOUTH);
                FindVisibleCells(board[indexSouth], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexEast != -1)
            {
                nextExpandRules.Add(Direction.EAST);
                FindVisibleCells(board[indexEast], board, dimension, nextExpandRules, depth - 1);
            }
            if (indexSouthEast != -1)
            {
                nextExpandRules.Add(Direction.EAST);
                nextExpandRules.Add(Direction.SOUTH);
                nextExpandRules.Add(Direction.SOUTHEAST);
                FindVisibleCells(board[indexSouthEast], board, dimension, nextExpandRules, depth - 1);
            }
        }

    }

    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    

    public void LoadPlayerPanelContent()
    {
        ui.LoadPlayerPanelContent(this);
        //ui.UpdateMovementPointValue(maxMovementPoints);
    }

    public void HidePlayerPanelContent()
    {
        ui.HidePlayerPanelContent();
    }

    public void ResetMovementPoints()
    {
        currentMovementPoints = maxMovementPoints;
    }

    public void DealDamage(int amount)
    {
        currentHealth -= amount;
        damageUI.StartAnimation(amount);
        if(currentHealth > 0)
        {
            SetAnimationState(AnimationState.Hit);
        }
        else
        {
            SetAnimationState(AnimationState.Death);
        }
           
    }

    public virtual void HideAllPossibilites()
    {
        HideMovementPossibilities();
    }

    public virtual void Die()
    {
        currentCell.SetCurrentCharacter(null);
        Destroy(gameObject);
        ui.OnCharacterDeath(this);
    }

    public int GetCurrentMovementPoints()
    {
        return currentMovementPoints;
    }


}
