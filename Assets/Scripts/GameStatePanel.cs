﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatePanel : MonoBehaviour {

    public Image archerIconRedPlayer;
    public Image assassinIconRedPalyer;
    public Image warriorIconRedPlayer;
    public Image kingIconRedPlayer;

    public Image archerIconBluePlayer;
    public Image assassinIconBluePalyer;
    public Image warriorIconBluePlayer;
    public Image kingIconBluePlayer;

    public GameObject leftIndicatorIcon;
    public GameObject rightIndicatorIcon;

    public Sprite deadArcherSprite;
    public Sprite deadAssasinSprite;
    public Sprite deadWarriorSprite;
    public Sprite deadKingSprite;

    public PlayerController redPlayer;
    public PlayerController bluePlayer;
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void ChangeDeadCharacterIcon(CharacterBehaviour dyingCharactor, PlayerController owenerPlayer)
    {
        if (owenerPlayer == redPlayer)
        {
            if(dyingCharactor.gameObject.GetComponent<ArcherBehaviour>() != null)
            {
                archerIconRedPlayer.sprite = deadArcherSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<AssassinBehaviour>() != null)
            {
                assassinIconRedPalyer.sprite = deadAssasinSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<WarriorBehaviour>() != null)
            {
                warriorIconRedPlayer.sprite = deadWarriorSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<KingBehaviour>() != null)
            {
                kingIconRedPlayer.sprite = deadKingSprite;
            }
        } 
        else if (owenerPlayer == bluePlayer)
        {
            if (dyingCharactor.gameObject.GetComponent<ArcherBehaviour>() != null)
            {
                archerIconBluePlayer.sprite = deadArcherSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<AssassinBehaviour>() != null)
            {
                assassinIconBluePalyer.sprite = deadAssasinSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<WarriorBehaviour>() != null)
            {
                warriorIconBluePlayer.sprite = deadWarriorSprite;
            }
            else if (dyingCharactor.gameObject.GetComponent<KingBehaviour>() != null)
            {
                kingIconBluePlayer.sprite = deadKingSprite;
            }
        }
    }

    public void SetTurn(PlayerController currentPlayer)
    {
        if(currentPlayer == redPlayer)
        {
            leftIndicatorIcon.SetActive(true);
            rightIndicatorIcon.SetActive(false);
        }
        else if (currentPlayer == bluePlayer)
        {
            leftIndicatorIcon.SetActive(false);
            rightIndicatorIcon.SetActive(true);
        }
    }
}
