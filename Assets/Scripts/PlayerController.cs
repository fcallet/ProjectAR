﻿using UnityEngine;


public class PlayerController : MonoBehaviour
{

    private GameManager gameManager;

    public ArcherBehaviour archer;
    public AssassinBehaviour assassin;
    public KingBehaviour king;
    public WarriorBehaviour warrior;

    public UIManager ui;

    // Use this for initialization
    void Start()
    {
        gameManager = GetComponentInParent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitArcher(CellProperty cell)
    {
        archer.transform.position = cell.transform.position;
        archer.gameObject.SetActive(true);
        archer.SetCurrentCell(cell);
        cell.SetCurrentCharacter(archer);
        cell.DisableHighlight();
        cell.SetAction(null);
        archer.ShowVisibleCells();
        gameManager.InitAssassin(this);
    }

    public void InitAssassin(CellProperty cell)
    {
        assassin.transform.position = cell.transform.position;
        assassin.gameObject.SetActive(true);
        assassin.SetCurrentCell(cell);
        cell.SetCurrentCharacter(assassin);
        cell.DisableHighlight();
        cell.SetAction(null);
        assassin.ShowVisibleCells();
        gameManager.InitWarrior(this);
    }

    public void InitWarrior(CellProperty cell)
    {
        warrior.transform.position = cell.transform.position;
        warrior.gameObject.SetActive(true);
        warrior.SetCurrentCell(cell);
        cell.SetCurrentCharacter(warrior);
        cell.DisableHighlight();
        cell.SetAction(null);
        warrior.ShowVisibleCells();
        gameManager.InitKing(this);
    }

    public void InitKing(CellProperty cell)
    {
        king.transform.position = cell.transform.position;
        king.gameObject.SetActive(true);
        king.SetCurrentCell(cell);
        cell.SetCurrentCharacter(king);
        cell.DisableHighlight();
        cell.SetAction(null);
        king.ShowVisibleCells();
        ui.FinishInitiation(this);
    }

    public void BeginTurn()
    {

        CellProperty archerCell = archer.GetCurrentCell();
        CellProperty assassinCell = assassin.GetCurrentCell();
        CellProperty warriorCell = warrior.GetCurrentCell();
        CellProperty kingCell = king.GetCurrentCell();

        archerCell.SetAction(gameManager.BeginSelectionAt);
        assassinCell.SetAction(gameManager.BeginSelectionAt);
        warriorCell.SetAction(gameManager.BeginSelectionAt);
        kingCell.SetAction(gameManager.BeginSelectionAt);

    }

    public CellProperty[] GetCells()
    {
        return gameManager.GetCells();
    }

    public Pair<int, int> GetBoardDimension()
    {
        return gameManager.GetBoardDimension();
    }

    public void ComputeVisibility()
    {
        archer.ShowVisibleCells();
        assassin.ShowVisibleCells();
        warrior.ShowVisibleCells();
        king.ShowVisibleCells();
    }

    public void ResetMovementPoints()
    {
        archer.ResetMovementPoints();
        assassin.ResetMovementPoints();
        warrior.ResetMovementPoints();
        king.ResetMovementPoints();
    }

    public void HideAllPossibilities()
    {
        archer.HideAllPossibilites();
        assassin.HideAllPossibilites();
        warrior.HideAllPossibilites();
        king.HideAllPossibilites();
    }

    public bool IsCharacterInTeam(CharacterBehaviour character)
    {
        if (character == archer || character == assassin || character == warrior || character == king)
            return true;
        else
            return false;

    }
}
