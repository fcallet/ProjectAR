﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerPanelState { PanelFolded, ContentHidden, ContentShown};

public class PlayerPanel : MonoBehaviour {

    public Text characterName;
    public Image characterIcon;
    public GameObject hp;
    public Image movementPointIcon;
    public Text movementPointValue;
    public Image attackIcon;
    public Image skillIcon;
    public Text roundNumber;
    public Slider hpBarSlider;

    public Sprite archerSprite;
    public Sprite assassinSprite;
    public Sprite warriorSprite;
    public Sprite kingSprite;

    public Sprite archerSkillSpriteActive;
    public Sprite assassinSkillSpriteActive;
    public Sprite warriorSkillSpriteActive;
    public Sprite kingSkillSprite;
    public Sprite commonAttackSpriteActive;
    public Sprite movementPointSpriteActive;

    public Sprite archerSkillSpriteDisabled;
    public Sprite assassinSkillIconSpriteDisabled;
    public Sprite warriorSkillIconSpriteDisabled;
    public Sprite commonAttackIconSpriteDisabled;
    public Sprite movementPointSpriteDisabled;

    public PlayerPanelState panelState;

    private CharacterBehaviour currentCharacter;
    private Animator animator;
    

    // Use this for initialization
    void Start () {

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InitializePanel()
    {
        animator = GetComponent<Animator>();
        animator.SetTrigger("showPlayerPanel");
        panelState = PlayerPanelState.ContentHidden;
    }

    public void HidePanel()
    {
        animator.SetTrigger("hidePlayerPanel");
        panelState = PlayerPanelState.PanelFolded;
    }

    public void StartTurn()
    {
        animator.SetTrigger("switchButton");
    }

    public void LoadContent(CharacterBehaviour character)
    {
        currentCharacter = character;
        if (panelState == PlayerPanelState.ContentHidden)
        {
            InitializeContent();
        }
        else if (panelState == PlayerPanelState.ContentShown)
        {
            if ((character.gameObject.GetComponent<ArcherBehaviour>() != null && characterName.text == "Archer")
             || (character.gameObject.GetComponent<AssassinBehaviour>() != null && characterName.text == "Assassin")
             || (character.gameObject.GetComponent<WarriorBehaviour>() != null && characterName.text == "Warrior")
             || (character.gameObject.GetComponent<KingBehaviour>() != null && characterName.text == "King"))
            {

            }
            else
            {
                UpdateNewContent();
            }
        }
        
    }

    public void InitializeContent()
    {

        ResetContentUI();
        animator.SetTrigger("loadContent");
        panelState = PlayerPanelState.ContentShown;

    }

    public void HideContent()
    {
        animator.SetTrigger("hideContent");
        panelState = PlayerPanelState.ContentHidden;
    }

    public void UpdateNewContent()
    {
        animator.SetTrigger("fadeOutContent");
    }

    public void FadeInContent()
    {
        ResetContentUI();
        animator.SetTrigger("fadeInContent");
        panelState = PlayerPanelState.ContentShown;
    }

    public void ResetContentUI()
    {
        attackIcon.sprite = commonAttackSpriteActive;
        movementPointIcon.sprite = movementPointSpriteActive;

        Button skillButton = skillIcon.GetComponent<Button>();
        Button attackButton = attackIcon.GetComponent<Button>();
        Button movementPointButton = movementPointIcon.GetComponent<Button>();

        skillButton.onClick.RemoveAllListeners();
        attackButton.onClick.RemoveAllListeners();
        movementPointButton.onClick.RemoveAllListeners();

        ArcherBehaviour archer;
        WarriorBehaviour warrior;
        AssassinBehaviour assassin;
        KingBehaviour king;

        movementPointValue.text = currentCharacter.GetCurrentMovementPoints().ToString();
        if ((archer = currentCharacter.gameObject.GetComponent<ArcherBehaviour>()) != null)
        {
            characterName.text = "Archer";
            characterIcon.sprite = archerSprite;
            hpBarSlider.maxValue = 5;
            hpBarSlider.value = currentCharacter.GetCurrentHealth();
            if (currentCharacter.GetCurrentMovementPoints() > 0)
            {
                skillIcon.sprite = archerSkillSpriteActive;
                skillButton.onClick.AddListener(archer.ShowBowAttackPossibilities);
                movementPointButton.onClick.AddListener(archer.ShowMovementPossibilities);
            }
            else
            {
                skillIcon.sprite = archerSkillSpriteDisabled;
                attackIcon.sprite = commonAttackIconSpriteDisabled;
                movementPointIcon.sprite = movementPointSpriteDisabled;
                skillButton.onClick.RemoveAllListeners();
                attackButton.onClick.RemoveAllListeners();
            }
        }
        else if ((assassin = currentCharacter.gameObject.GetComponent<AssassinBehaviour>()) != null)
        {
            characterName.text = "Assassin";
            characterIcon.sprite = assassinSprite;
            hpBarSlider.maxValue = 4;
            hpBarSlider.value = currentCharacter.GetCurrentHealth();
            if (currentCharacter.GetCurrentMovementPoints() > 0)
            {
                skillIcon.sprite = assassinSkillSpriteActive;
                movementPointButton.onClick.AddListener(assassin.ShowMovementPossibilities);
                attackButton.onClick.AddListener(assassin.ShowAttackPossibilities);
            }
            else
            {
                skillIcon.sprite = assassinSkillIconSpriteDisabled;
                attackIcon.sprite = commonAttackIconSpriteDisabled;
                movementPointIcon.sprite = movementPointSpriteDisabled;
                skillButton.onClick.RemoveAllListeners();
                attackButton.onClick.RemoveAllListeners();
            }
            
          
        }
        else if ((warrior = currentCharacter.gameObject.GetComponent<WarriorBehaviour>()) != null)
        {
            characterName.text = "Warrior";
            characterIcon.sprite = warriorSprite;
            hpBarSlider.maxValue = 8;
            hpBarSlider.value = currentCharacter.GetCurrentHealth();
            if (currentCharacter.GetCurrentMovementPoints() > 0)
            {
                skillIcon.sprite = warriorSkillSpriteActive;
                attackButton.onClick.AddListener(warrior.ShowAttackPossibilities);
                movementPointButton.onClick.AddListener(warrior.ShowMovementPossibilities);
            }
            else
            {
                skillIcon.sprite = warriorSkillIconSpriteDisabled;
                attackIcon.sprite = commonAttackIconSpriteDisabled;
                movementPointIcon.sprite = movementPointSpriteDisabled;
                skillButton.onClick.RemoveAllListeners();
                attackButton.onClick.RemoveAllListeners();
            }
            
            
        }
        else if ((king = currentCharacter.gameObject.GetComponent<KingBehaviour>()) != null)
        {
            characterName.text = "King";
            characterIcon.sprite = kingSprite;
            skillIcon.sprite = kingSkillSprite;
            hpBarSlider.maxValue = 6;
            hpBarSlider.value = currentCharacter.GetCurrentHealth();
            if (currentCharacter.GetCurrentMovementPoints() > 0)
            {
                movementPointButton.onClick.AddListener(king.ShowMovementPossibilities);
                attackButton.onClick.AddListener(king.ShowAttackPossibilities);
            }
            else
            {
                attackIcon.sprite = commonAttackIconSpriteDisabled;
                movementPointIcon.sprite = movementPointSpriteDisabled;
                skillButton.onClick.RemoveAllListeners();
                attackButton.onClick.RemoveAllListeners();
            }
        }
    }

    /*
    public void UpdateMovementPointValue(int currentMovementPoint)
    {
        movementPointValue.text = currentMovementPoint.ToString();
    }*/

    public void SetNewRoundNumber(int number)
    {
        roundNumber.text = number.ToString();
    }


}
