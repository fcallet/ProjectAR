﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageUI : MonoBehaviour {

    private Animator damageUIAnimator;
    private Text damageText;

	// Use this for initialization
	void Start () {

        damageText = GetComponentInChildren<Text>();
        damageUIAnimator = GetComponent<Animator>();
        damageUIAnimator.SetTrigger("Invisible");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(Camera.main.transform.position - v);
        transform.Rotate(0, 180, 0);
    }

    public void StartAnimation(int amountDamage)
    {
        gameObject.SetActive(true);
        damageText.text = "-" + amountDamage.ToString();
        damageUIAnimator.SetTrigger("Visible");
        StartCoroutine(StopWithDelay());
    }

    private IEnumerator StopWithDelay()
    {
        yield return new WaitForSeconds(1.5f);
        damageUIAnimator.SetTrigger("Invisible");

    }
}
