﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public GameManager gameManager;
    public GameObject homePagePanel;
    public GameObject gameOverPanel;

    public GameObject initiationPanel;
    public GameObject redPlayerInitPanel;
    public GameObject bluePlayerInitPanel;

    public GameStatePanel gameStatePanel;
    public PlayerPanel redPlayerPanel;
    public PlayerPanel bluePlayerPanel;
    public PlayerController redPlayer;
    public PlayerController bluePlayer;

    public Image wonKingIcon;
    public Image wonWarriorIcon;
    public Image wonAssassinIcon;
    public Image wonArcherIcon;

    public Sprite redKingIcon;
    public Sprite redWarriorIcon;
    public Sprite redAssassinIcon;
    public Sprite redArcherIcon;

    public Sprite blueKingIcon;
    public Sprite blueWarriorIcon;
    public Sprite blueAssassinIcon;
    public Sprite blueArcherIcon;

    [HideInInspector]
    public PlayerController currentPlayer;
    private PlayerPanel currentPlayerPanel;
    private CharacterBehaviour currentCharacter;
    private Animator redPlayerInitPanelAnim;
    private Animator bluePlayerInitPanelAnim;

    // Use this for initialization
    void Start () {
        DisplayHomePage();
        redPlayerInitPanelAnim = redPlayerInitPanel.GetComponent<Animator>();
        bluePlayerInitPanelAnim = bluePlayerInitPanel.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisplayHomePage()
    {
        homePagePanel.gameObject.SetActive(true);
        
    }

    public void DisplayInitiationScreen()
    {
        initiationPanel.gameObject.SetActive(true);
        homePagePanel.gameObject.SetActive(false);
        ShowPlayerInitPanel(redPlayer);

    }


    public void OnClickStartGame()
    {
        DisplayInitiationScreen();
    }

    public void OnClickExitGame()
    {
        
    }

    public void ShowPlayerInitPanel(PlayerController player)
    {
        if(player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("showInitPanel");
        } else 
        {
            bluePlayerInitPanelAnim.SetTrigger("showInitPanel");
        }
    }

    public void OnClickStartInitiation(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("startInitiation");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("startInitiation");
        }
        gameManager.BeginInitiation(player);
    }

    public void BeginPlacingArcher(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("showArcherIcon");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("showArcherIcon");
        }
    }

    public void BeginPlacingAssassin(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("showAssassinIcon");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("showAssassinIcon");
        }
    }

    public void BeginPlacingWarrior(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("showWarriorIcon");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("showWarriorIcon");
        }
    }

    public void BeginPlacingKing(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("showKingIcon");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("showKingIcon");
        }
    }

    public void FinishInitiation(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("finishInitiation");
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("finishInitiation");
        }
    }

    public void OnClickDoneInitiation(PlayerController player)
    {
        if (player == redPlayer)
        {
            redPlayerInitPanelAnim.SetTrigger("hideInitPanel");
            ShowPlayerInitPanel(bluePlayer);
        }
        else
        {
            bluePlayerInitPanelAnim.SetTrigger("hideInitPanel");

        }

        gameManager.ConfirmInitiation(player);
    }



    public void LoadPlayerPanel(PlayerController player)
    {
        if (player == redPlayer)
        {
            currentPlayerPanel = redPlayerPanel;
            currentPlayer = redPlayer;
            redPlayerInitPanel.SetActive(false);
        }
        else if (player == bluePlayer)
        {
            currentPlayerPanel = bluePlayerPanel;
            currentPlayer = bluePlayer;
            bluePlayerInitPanel.SetActive(false);
            initiationPanel.SetActive(false);
        }

        currentPlayerPanel.gameObject.SetActive(true);
        currentPlayerPanel.InitializePanel();
        gameStatePanel.SetTurn(player);
    }

    public void HidePlayerPanel()
    {
        currentPlayerPanel.HidePanel();
    }

    public void LoadPlayerPanelContent(CharacterBehaviour character)
    {
        currentCharacter = character;
        currentPlayerPanel.LoadContent(currentCharacter);
        
    }

    public void HidePlayerPanelContent()
    {
        currentPlayerPanel.HideContent();
    }

    /*
    public void UpdateMovementPointValue(int value)
    {
        currentPlayerPanel.UpdateMovementPointValue(value);
    }*/

    public void ResetContentUI()
    {
        currentPlayerPanel.ResetContentUI();
    }

    public void OnCharacterDeath(CharacterBehaviour dyingCharacter)
    {
        if(currentPlayer == redPlayer)
        {
            gameStatePanel.ChangeDeadCharacterIcon(dyingCharacter, bluePlayer);
        } 
        else if(currentPlayer == bluePlayer)
        {
            gameStatePanel.ChangeDeadCharacterIcon(dyingCharacter, redPlayer);
        }
    }

    public void SetNewRoundNumber(int number)
    {
        currentPlayerPanel.SetNewRoundNumber(number);
    }

    public void OnClickSkillButton()
    {

    }

    public void OnClickAttackButton()
    {

    }

    public void OnClickStartTurn()
    {
        currentPlayerPanel.StartTurn();
        gameManager.UnmaskVisibilityForNewTurn(currentPlayer);
    }

    public void OnClickDoneButton()
    {
        gameManager.OnFinishTurn();
        if (currentPlayerPanel.panelState == PlayerPanelState.ContentHidden)
        {
            currentPlayerPanel.HidePanel();
        }
        else if (currentPlayerPanel.panelState == PlayerPanelState.ContentShown)
        {
            currentPlayerPanel.HideContent();
        }
        if(currentPlayer == redPlayer)
        {
            redPlayer.HideAllPossibilities();
            gameManager.BeginTurn(bluePlayer);
        }
        else if(currentPlayer == bluePlayer)
        {
            bluePlayer.HideAllPossibilities();
            gameManager.BeginTurn(redPlayer);
        }
    }

    public void GameOver()
    {
        if(currentPlayer == redPlayer)
        {
            wonKingIcon.sprite = redKingIcon;
            wonWarriorIcon.sprite = redWarriorIcon;
            wonAssassinIcon.sprite = redAssassinIcon;
            wonArcherIcon.sprite = redArcherIcon;
        }
        else if (currentPlayer == bluePlayer)
        {
            wonKingIcon.sprite = blueKingIcon;
            wonWarriorIcon.sprite = blueWarriorIcon;
            wonAssassinIcon.sprite = blueAssassinIcon;
            wonArcherIcon.sprite = blueArcherIcon;
        }

        if (currentPlayerPanel.panelState == PlayerPanelState.ContentHidden)
        {
            currentPlayerPanel.HidePanel();
        }
        else if (currentPlayerPanel.panelState == PlayerPanelState.ContentShown)
        {
            currentPlayerPanel.HideContent();
        }

        gameOverPanel.SetActive(true);



    }

    public void OnClickRestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        DisplayInitiationScreen();
    }


}
