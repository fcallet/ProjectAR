﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssassinBehaviour : CharacterBehaviour {

    public Material assassinDefaultMaterial;
    public Material assassinOutlineMaterial;

    private SkinnedMeshRenderer[] assassinMeshRenderers;
    private List<CellProperty> attackPossibilities;
    private CellProperty targetCell;

    // Use this for initialization
    protected override void Start ()
    {
        base.Start();
        maxMovementPoints = 2;
        maxHealth = 4;
        currentHealth = maxHealth;
        currentMovementPoints = maxMovementPoints;
        targetCell = null;
        assassinMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
    }
	
	// Update is called once per frame
	protected override void Update ()
    {
        base.Update();
	}

    public override void SetOutline(bool isOutlined)
    {
        if (isOutlined)
        {
            assassinMeshRenderers[0].material = assassinOutlineMaterial;
            assassinMeshRenderers[1].material = assassinOutlineMaterial;
            assassinMeshRenderers[2].material = assassinOutlineMaterial;
        }
        else
        {
            assassinMeshRenderers[0].material = assassinDefaultMaterial;
            assassinMeshRenderers[1].material = assassinDefaultMaterial;
            assassinMeshRenderers[2].material = assassinDefaultMaterial;
        }
    }

    public void ShowAttackPossibilities()
    {
        HideAllPossibilites();
        CellProperty[] board = owner.GetCells();
        Pair<int, int> dimension = owner.GetBoardDimension();
        attackPossibilities = FindKnifeAttackPossibilities(board, dimension);
    }

    public void HideAttackPossibilities()
    {
        if (attackPossibilities != null)
        {
            foreach (CellProperty cell in attackPossibilities)
            {
                cell.DisableHighlight();
                if (cell.GetCurrentCharacter() == null || !owner.IsCharacterInTeam(cell.GetCurrentCharacter()))
                    cell.SetAction(null);
            }
        }
    }

    public void AttackWithKnife(CellProperty target)
    {
        transform.LookAt(target.GetCurrentCharacter().transform);
        HideAllPossibilites();
        animator.SetBool("Attack", true);
        targetCell = target;
    }

    public void AttackPlayer()
    {
        const int amount = 4;
        targetCell.GetCurrentCharacter().DealDamage(amount);
        targetCell = null;
    }

    public void StopKnifeAttackAnimation()
    {
        animator.SetBool("Attack", false);
        currentMovementPoints = 0;
        ui.ResetContentUI();
    }

    public override void HideAllPossibilites()
    {
        base.HideAllPossibilites();
        HideAttackPossibilities();
    }

    private List<CellProperty> FindKnifeAttackPossibilities(CellProperty[] board, Pair<int, int> dimension)
    {
        List<CellProperty> possibilities = new List<CellProperty>();
        int indexNorth = (currentCell.indexInList % dimension.First != 9) ? currentCell.indexInList + 1 : -1;
        int indexSouth = (currentCell.indexInList % dimension.First != 0) ? currentCell.indexInList - 1 : -1;
        int indexWest = (currentCell.indexInList >= dimension.First) ? currentCell.indexInList - dimension.First : -1;
        int indexEast = (dimension.First * dimension.Second - currentCell.indexInList > dimension.First) ? currentCell.indexInList + dimension.First : -1;
        int indexNorthEast = (indexNorth != -1 && indexEast != -1) ? currentCell.indexInList + dimension.First + 1 : -1;
        int indexNorthWest = (indexNorth != -1 && indexWest != -1) ? currentCell.indexInList - dimension.First + 1 : -1;
        int indexSouthWest = (indexSouth != -1 && indexWest != -1) ? currentCell.indexInList - dimension.First - 1 : -1;
        int indexSouthEast = (indexSouth != -1 && indexEast != -1) ? currentCell.indexInList + dimension.First - 1 : -1;

        if (indexNorth != -1)
        {
            if (board[indexNorth].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorth].GetCurrentCharacter()))
            {
                board[indexNorth].HighlightGreen();
                board[indexNorth].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexNorth].HighlightRed();
            }
            possibilities.Add(board[indexNorth]);

        }
        if (indexSouth != -1)
        {
            if (board[indexSouth].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouth].GetCurrentCharacter()))
            {
                board[indexSouth].HighlightGreen();
                board[indexSouth].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexSouth].HighlightRed();
            }
            possibilities.Add(board[indexSouth]);
        }
        if (indexWest != -1)
        {
            if (board[indexWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexWest].GetCurrentCharacter()))
            {
                board[indexWest].HighlightGreen();
                board[indexWest].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexWest].HighlightRed();
            }
            possibilities.Add(board[indexWest]);
        }
        if (indexEast != -1)
        {
            if (board[indexEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexEast].GetCurrentCharacter()))
            {
                board[indexEast].HighlightGreen();
                board[indexEast].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexEast].HighlightRed();
            }
            possibilities.Add(board[indexEast]);
        }
        if (indexNorthEast != -1)
        {
            if (board[indexNorthEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorthEast].GetCurrentCharacter()))
            {
                board[indexNorthEast].HighlightGreen();
                board[indexNorthEast].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexNorthEast].HighlightRed();
            }
            possibilities.Add(board[indexNorthEast]);
        }
        if (indexNorthWest != -1)
        {
            if (board[indexNorthWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexNorthWest].GetCurrentCharacter()))
            {
                board[indexNorthWest].HighlightGreen();
                board[indexNorthWest].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexNorthWest].HighlightRed();
            }
            possibilities.Add(board[indexNorthWest]);
        }
        if (indexSouthEast != -1)
        {
            if (board[indexSouthEast].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouthEast].GetCurrentCharacter()))
            {
                board[indexSouthEast].HighlightGreen();
                board[indexSouthEast].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexSouthEast].HighlightRed();
            }
            possibilities.Add(board[indexSouthEast]);
        }
        if (indexSouthWest != -1)
        {
            if (board[indexSouthWest].GetCurrentCharacter() != null && !owner.IsCharacterInTeam(board[indexSouthWest].GetCurrentCharacter()))
            {
                board[indexSouthWest].HighlightGreen();
                board[indexSouthWest].SetAction(AttackWithKnife);
            }
            else
            {
                board[indexSouthWest].HighlightRed();
            }
            possibilities.Add(board[indexSouthWest]);
        }



        return possibilities;
    }

}
