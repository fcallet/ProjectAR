﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VirtualButtonController : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject virtualButtonObj;
    public GameManager gameManager;
    //public UIManager ui;
    //public PlayerController currentPlayer;
    private CellProperty collisionCell;

	// Use this for initialization
	void Start () {
        //virtualButtonObj = GameObject.Find("UnmaskBtn");
        virtualButtonObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        /*
        Debug.Log("VB Pressed");

        if (collisionCell != null)
        {
            gameManager.UnmaskVisibilityOfCells(collisionCell);
            collisionCell = null;
        }
        else
        {
            Debug.Log("cell is null !");
        }*/
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.Log("VB Released");
        //gameManager.MaskVisibilityOfCells(currentPlayer);

    }

    public void OnTangibleTargetCollision(CellProperty cell)
    {
        collisionCell = cell;
        Debug.Log("cell collision : " + cell.x + ", " + cell.y);
    }


}
