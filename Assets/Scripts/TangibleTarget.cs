﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TangibleTarget : MonoBehaviour {

    public GameManager gameManager;
    //public VirtualButtonController vbController;
    //public VirtualButtonController bluePlayerVBController;
    //public UIManager ui;
    //public PlayerController redPlayer;
    //public PlayerController bluePlayer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<CellProperty>() != null)
        {
            gameManager.OnTangibleTargetCollision(other.GetComponent<CellProperty>());
            /*
            if(ui.currentPlayer == redPlayer)
            {
                redPlayerVBController.OnTangibleTargetCollision(other.GetComponent<CellProperty>());
            }
            else if(ui.currentPlayer == bluePlayer)
            {
                bluePlayerVBController.OnTangibleTargetCollision(other.GetComponent<CellProperty>());
            }*/
        }
    }
}
